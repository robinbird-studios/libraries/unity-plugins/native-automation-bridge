namespace RobinBirdStudios.NativeAutomationBridge
{
    using UnityEngine;

    public class AndroidUIData
    {
        public int x;
        public int y;
        public int width;
        public int height;
        public string name;

#if UNITY_EDITOR || UNITY_ANDROID
        public AndroidJavaObject ToJavaObject()
        {
            var obj = new AndroidJavaObject("com.robinbirdstudios.nativeautomationbridge.DataClass");
            obj.Set("x", x);
            obj.Set("y", y);
            obj.Set("width", width);
            obj.Set("height", height);
            obj.Set("name", name);
            return obj;
        }
#endif
    }
}
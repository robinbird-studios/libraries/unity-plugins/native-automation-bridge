namespace RobinBirdStudios.NativeAutomationBridge
{
    using System.Collections;
    using System.Collections.Generic;
    using JetBrains.Annotations;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class AndroidUICreator
    {
        public IEnumerator UpdateUIEverySecond()
        {
            // Refresh UI every second
            var waitForSeconds = new WaitForSeconds(1);
            while (true)
            {
                Create();
                yield return waitForSeconds;
            }
        }

        public void Create()
        {
            Debug.Log("Getting Unity Player class");
            // get the UnityPlayerClass
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            Debug.Log("Getting current Activity");
            // get the current activity
            var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            var buttons = Object.FindObjectsOfType<Button>();
            var dataList = new List<AndroidJavaObject>();

            foreach (Button button in buttons)
            {
                var rectTransform = button.GetComponent<RectTransform>();

                var worldCorners = new Vector3[4];
                rectTransform.GetWorldCorners(worldCorners);

                var pixelRect = new Rect(
                    worldCorners[0].x,
                    worldCorners[0].y,
                    worldCorners[2].x - worldCorners[0].x,
                    worldCorners[2].y - worldCorners[0].y);
                
                var data = new AndroidUIData()
                {
                    x = (int)pixelRect.x,
                    y = (int)pixelRect.y,
                    width = (int)pixelRect.width,
                    height = (int)pixelRect.height,
                    name = button.gameObject.name
                };
                
                dataList.Add(data.ToJavaObject());
            }

            Debug.Log("Creating array");
            AndroidJavaObject javaObject = JavaArrayFromCS(dataList.ToArray(), "com.robinbirdstudios.nativeautomationbridge.DataClass");
            
            Debug.Log("Calling method");
            activity.Call("CreateButton", javaObject);
        }

        [UsedImplicitly]
        public void UiCreatorButtonPressed(string name)
        {
            Debug.Log("Got button press from: " + name);
            
            var obj = GameObject.Find(name);
            Button button = obj.GetComponent<Button>();

            RoutineDispatcher.Instance.StartCoroutine(ClickButton(button));
        }

        private static IEnumerator ClickButton(Button b) {
            Debug.Log("Tapping on Button: " + b.gameObject.name);
            var pointer = new PointerEventData(EventSystem.current);
 
            ExecuteEvents.Execute(b.gameObject, pointer, ExecuteEvents.pointerEnterHandler);
            ExecuteEvents.Execute(b.gameObject, pointer, ExecuteEvents.pointerDownHandler);
            yield return new WaitForSeconds(0.1f);
            ExecuteEvents.Execute(b.gameObject, pointer, ExecuteEvents.pointerUpHandler);
            ExecuteEvents.Execute(b.gameObject, pointer, ExecuteEvents.pointerClickHandler);
        }
        
        private AndroidJavaObject JavaArrayFromCS(AndroidJavaObject [] values, string name) {
            AndroidJavaClass arrayClass  = new AndroidJavaClass("java.lang.reflect.Array");
            AndroidJavaObject arrayObject = arrayClass.CallStatic<AndroidJavaObject>("newInstance", new AndroidJavaClass(name), values.Length);
            for (int i=0; i<values.Length; ++i) {
                arrayClass.CallStatic("set", arrayObject, i, values[i]);
            }
            
            return arrayObject;
        }
    }
}
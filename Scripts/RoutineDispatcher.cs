﻿namespace RobinBirdStudios.NativeAutomationBridge
{
    using UnityEngine;

    public class RoutineDispatcher : MonoBehaviour
    {
        private static RoutineDispatcher instance;
    
        public static RoutineDispatcher Instance
        {
            get
            {
                if (instance == null)
                {
                    var obj = new GameObject("RoutineDispatcher");
                    instance = obj.AddComponent<RoutineDispatcher>();
                    DontDestroyOnLoad(instance);
                }

                return instance;
            }
        }
    }
}

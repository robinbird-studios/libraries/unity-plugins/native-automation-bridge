namespace RobinBirdStudios.NativeAutomationBridge
{
    using System;
    using Firebase.TestLab;
    using JetBrains.Annotations;
    using UnityEngine;

    public class TestLabStarter : MonoBehaviour
    {
        private TestLabManager testLabManager;
        private AndroidUICreator uiCreator;
        private Coroutine uiCreationRoutine;
        
        private const int BootTimeTest = 1;

        private const int FrameRateAfterOneSecondTest = 2;
        private const int FrameRateAfterThreeSecondsTest = 3;

        private void Awake()
        {
            // At the moment only Android is supported
            if (Application.platform != RuntimePlatform.Android)
            {
                Destroy(this);
                return;
            }

            gameObject.name = "FirebaseTestLab";
        }

        private void Start() {
            testLabManager = TestLabManager.Instantiate();
            uiCreator = new AndroidUICreator();

            if (IsTestLabTest())
            {
                if (uiCreationRoutine == null)
                {
                    Debug.Log("Detected TestLabs test and activating UI translator");
                    uiCreationRoutine = RoutineDispatcher.Instance.StartCoroutine(uiCreator.UpdateUIEverySecond());
                }
            }
        }

        private void Update()
        {
            // only change the behavior of the app if there is a scenario being tested
            if (testLabManager.IsTestingScenario)
            {
                switch (testLabManager.ScenarioNumber)
                {
                    case BootTimeTest:
                        // print out the result and complete
                        testLabManager.LogToResults("Time to boot: " +
                                                    Time.realtimeSinceStartup +
                                                    "\n");
                        testLabManager.NotifyHarnessTestIsComplete();
                        break;
                    case FrameRateAfterOneSecondTest:
                        CheckFramerate(1.0f);
                        break;
                    case FrameRateAfterThreeSecondsTest:
                        CheckFramerate(3.0f);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public bool IsTestLabTest()
        {
            Debug.Log("Getting Unity Player class");
            // get the UnityPlayerClass
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

            Debug.Log("Getting current Activity");
            // get the current activity
            var activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            return activity.Call<bool>("IsTestLabTest");
        }

        /// <summary>
        /// Called via UnitySendMessage from Android Activity to signal that proxy element has been pressed
        /// </summary>
        /// <param name="info"></param>
        [UsedImplicitly]
        public void UiCreatorButtonPressed(string info)
        {
            uiCreator.UiCreatorButtonPressed(info);
        }

        private void CheckFramerate(float afterTime) {
            if (!(Time.time > afterTime)) return;

            const string s = "Number frames after {0} seconds: {1}\n";
            string output = string.Format(s, afterTime, Time.frameCount);

            testLabManager.LogToResults(output);
            testLabManager.NotifyHarnessTestIsComplete();
        }
    }
}